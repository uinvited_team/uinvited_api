﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace uInvited_Api.Models.Database
{
    public class DbConnection:IDisposable
    {
        dbHelper db = new dbHelper();

        #region [Create Event]

        /// <summary>
        /// Inserts new event created by user
        /// </summary>
        /// <param name="_event"></param>
        /// <returns>string</returns>
        public string CreateEvent(Events _event)
        {
            string eventId = string.Empty;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("userId", _event.UserId);
                parameters.Add("eventType", _event.Type);
                parameters.Add("eventTitle", _event.Title);
                parameters.Add("templateId", _event.TemplateId);
                parameters.Add("eventMessage", _event.InvitationMessage);

                eventId = db.ExecuteScalar(parameters, "sp_createEvent");
            }

            return eventId;
        }
        #endregion

        #region [Create Reference]

        /// <summary>
        /// inserts reference of event or subevent
        /// </summary>
        /// <param name="references"></param>
        /// <returns>object</returns>
        public References CreateReference(References references)
        {
            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("eventId", references.EventId);
                parameters.Add("subEventId", references.SubEventId);
                parameters.Add("userId",references.UserId);
                parameters.Add("referenceNo", references.ReferenceNo);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_createReference");

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        references.ReferencesId = row["referenceId"].ToString();
                        references.ReferenceNo = row["refNo"].ToString();
                    }
                }
            }

            return references;
        }
        #endregion

        #region [Create SubEvent]

        /// <summary>
        /// insert subEvent of user 
        /// </summary>
        /// <param name="subEvent"></param>
        /// <returns>string</returns>
        public string CreateSubEvent(SubEvent subEvent)
        {
            string subEventId = string.Empty;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("evntId", subEvent.EventId);
                parameters.Add("subEventName", subEvent.SubEventName);
                parameters.Add("subEventDate", subEvent.SubEventDate);
                parameters.Add("strtTime", subEvent.StartTime);
                parameters.Add("endTime", subEvent.EndTime);
                parameters.Add("address", subEvent.Address);
                parameters.Add("lon", subEvent.Longitude);
                parameters.Add("lat", subEvent.Latitude);

                subEventId = db.ExecuteScalar(parameters, "sp_createSubEvent");
            }
            return subEventId;
        }
        #endregion

        #region [Create Template]

        /// <summary>
        /// inserts new template
        /// </summary>
        /// <param name="template"></param>
        /// <returns>string</returns>
        public string CreateTemplate(Template template)
        {
            string templateId = string.Empty;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("templateName", template.Name);
                parameters.Add("templateHtml", template.Html);

                templateId = db.ExecuteScalar(parameters, "sp_createTemplate");
            }
            return templateId;
        }
        #endregion

        #region [Delete Event]

        /// <summary>
        /// deletes event created by user
        /// if no references is created for event
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns>bool</returns>
        public bool DeleteEvent(string eventId,string userId)
        {
            bool isDeleted = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("evntId", eventId);
                parameters.Add("userId", userId);

                isDeleted = db.ExecuteNonQuery(parameters, "sp_deleteEvent");
            }
            return isDeleted;
        }
        #endregion

        #region [Delete SubEvent]

        /// <summary>
        /// deletes subevent of user 
        /// if no references no is generated for it
        /// </summary>
        /// <param name="subEventId"></param>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public bool DeleteSubEvent(string subEventId, string eventId)
        {
            bool isDeleted = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("subEventId", subEventId);
                parameters.Add("evntId", eventId);

                isDeleted = db.ExecuteNonQuery(parameters, "sp_deleteSubEvent");
            }
            return isDeleted;
        }

        #endregion

        #region [Delete Template]

        /// <summary>
        /// changes template active status
        /// from true to false
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public bool DeleteTemplate(string templateId)
        {
            bool isDeleted = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("templateId", templateId);
                parameters.Add("templateActive", "false");

                isDeleted = db.ExecuteNonQuery(parameters, "sp_deleteTemplate");
            }
            return isDeleted;
        }
        #endregion

        #region [Get Reference Detail]

        /// <summary>
        /// gets reference detail on base of reference no
        /// </summary>
        /// <param name="referenceNo"></param>
        /// <returns></returns>
        public References GetReferenceDetail(string referenceNo)
        {
            References references = new References();

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("referenceNo", referenceNo);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_getReferenceDetail");

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        references.ReferencesId = row["id"].ToString();
                        references.UserId = row["refr_user_id"].ToString();
                        references.EventId = row["refr_event_id"].ToString();
                        references.SubEventId = row["refr_sub_event_id"].ToString();
                        references.UpdatedDate = row["refr_updated_date"].ToString();
                    }
                }
            }
            return references;
        }
        #endregion

        #region [Get Template By Id]

        /// <summary>
        /// gets template by Id
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns>object</returns>
        public Template GetTemplateById(string templateId)
        {
            Template template = new Template();

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("templateId", templateId);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_getTemplateById");

                if (dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        template.TemplateId = row["id"].ToString();
                        template.Name = row["tmpl_name"].ToString();
                        template.Html = row["tmpl_html"].ToString();
                        template.Active = bool.Parse((row["tmpl_active"] ?? "false").ToString());
                        template.UpdatedDate = row["tmpl_updated_date"].ToString();
                    }
                }
            }
            return template;
        }
        #endregion

        #region [Get Templates]

        /// <summary>
        /// get template list which are greater than provided templateId
        /// in bunch of limit configured
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns>list</returns>
        public List<Template> GetTemplates(string templateId)
        {
            List<Template> templateList = new List<Template>();

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                string limit = ConfigurationManager.AppSettings["limit"];

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("templateId", templateId);
                parameters.Add("lmt", limit);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_getTemplates");

                if (dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        Template template = new Template();
                        template.TemplateId = row["id"].ToString();
                        template.Name = row["tmpl_name"].ToString();
                        template.Html = row["tmpl_html"].ToString();
                        template.Active = bool.Parse((row["tmpl_active"] ?? "false").ToString());
                        template.UpdatedDate = row["tmpl_updated_date"].ToString();

                        templateList.Add(template);
                    }
                }
            }
            return templateList;
        }
        #endregion

        #region [Get User's Events]

        /// <summary>
        /// gets list of event of user
        /// </summary>
        /// <returns>list</returns>
        public List<Events> GetUserEvent()
        {
            List<Events> eventList = new List<Events>();

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("userId", Utility.Utility.header);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_getUserEvent");

                if (dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        Events _event = new Events();
                        _event.EventId = row["id"].ToString();
                        _event.Type = row["evnt_type"].ToString();
                        _event.Title = row["evnt_title"].ToString();
                        _event.TemplateId = row["evnt_template_id"].ToString();
                        _event.InvitationMessage = row["evnt_invitation_message"].ToString();
                        _event.UpdatedDate = row["evnt_updated_date"].ToString();

                        eventList.Add(_event);
                    }
                }
            }
            return eventList;
        }
        #endregion

        #region [Get User Id]

        /// <summary>
        /// get user's id
        /// </summary>
        /// <returns>string</returns>
        public string GetUserId()
        {
            string userId = string.Empty;

            if(!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("mobileNo", Utility.Utility.header);

                userId = db.ExecuteScalar(parameters, "sp_getUserId");
            }
            return userId;
        }
        #endregion

        #region [Get OTP]

        /// <summary>
        /// gets user otp & no of attempt
        /// </summary>
        /// <returns>object</returns>
        public Users GetOTP()
        {
            Users user = null;

            if(!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("mob", Utility.Utility.header);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_getUserOtp");

                if(dt.Rows.Count>0)
                {
                    user = new Users();
                    foreach(DataRow row in dt.Rows)
                    {
                        user.MobileNo = Utility.Utility.header;
                        user.Otp = row["usr_otp"].ToString();
                        user.Attempt = row["usr_attempt"].ToString();
                        user.TotalAttempt = row["usr_total_attempt"].ToString();
                        user.IsBlock = bool.Parse((row["usr_block"] ?? "false").ToString());
                        user.UpdatedDate = row["usr_updated_date"].ToString();
                    }
                }
            }
            return user;
        }
        #endregion

        #region [Get SubEvent]

        /// <summary>
        /// get list of subevent of provided event
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns>list</returns>
        public List<SubEvent> GetSubEvent(string eventId)
        {
            List<SubEvent> subEventList = new List<SubEvent>();

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("eventId", eventId);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_getUserSubEvent");

                if (dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        SubEvent subEvent = new SubEvent();
                        subEvent.SubEventId = row["id"].ToString();
                        subEvent.SubEventName = row["suev_sub_event_name"].ToString();
                        subEvent.SubEventDate = row["suev_sub_event_date"].ToString();
                        subEvent.StartTime = row["suev_start_time"].ToString();
                        subEvent.EndTime = row["suev_end_time"].ToString();
                        subEvent.Address = row["suev_sub_event_address"].ToString();
                        subEvent.Longitude = row["suev_longitude"].ToString();
                        subEvent.Latitude = row["suev_latitude"].ToString();
                        subEvent.UpdatedDate = row["suev_updated_date"].ToString();

                        subEventList.Add(subEvent);
                    }
                }
            }
            return subEventList;
        }
        #endregion

        #region [Get Invited People]

        /// <summary>
        /// get list of people invited in particular event or subevent
        /// </summary>
        /// <param name="invitedPeople"></param>
        /// <returns>list</returns>
        public List<InvitedPeople> GetInvitedPeople(InvitedPeople invitedPeople)
        {
            List<InvitedPeople> invitedPeopleList = new List<InvitedPeople>();

            if(!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("userId", invitedPeople.UserId);
                parameters.Add("eventId", invitedPeople.EventId);
                parameters.Add("subEventId", invitedPeople.SubEventId);

                DataTable dt = new DataTable();
                dt = db.ExecuteReader(parameters, "sp_getInvitedPeople");

                if (dt.Rows.Count > 0)
                {
                    foreach(DataRow row in dt.Rows)
                    {
                        InvitedPeople people = new InvitedPeople();
                        people.UserId = invitedPeople.UserId;
                        people.EventId = invitedPeople.EventId;
                        people.SubEventId = row["einp_sub_event_id"].ToString();
                        people.InvitedUser = row["einp_invited_user"].ToString();
                        people.Accepted = bool.Parse((row["einp_accepted"]??"false").ToString());
                        people.UpdatedDate = row["einp_updated_date"].ToString();

                        invitedPeopleList.Add(people);
                    }
                }
            }
            return invitedPeopleList;
        }
        #endregion

        #region [Invite People]

        /// <summary>
        /// invite people for a partical event or subevent
        /// </summary>
        /// <param name="invitedPeople"></param>
        /// <returns>bool</returns>
        public bool InvitePeople(InvitedPeople invitedPeople)
        {
            bool invited = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("userId", invitedPeople.UserId);
                parameters.Add("eventId", invitedPeople.EventId);
                parameters.Add("subEventId", invitedPeople.SubEventId);
                parameters.Add("invitedUser", invitedPeople.InvitedUser);

                invited = db.ExecuteNonQuery(parameters, "sp_invitePeople");

            }
            return invited;
        }
        #endregion

        #region [Register User]

        /// <summary>
        /// register new user
        /// </summary>
        /// <param name="otp"></param>
        /// <returns>string</returns>
        public string RegisterUser(string otp)
        {
            string userId = string.Empty;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("mob", Utility.Utility.header);
                parameters.Add("otp", otp);

                userId = db.ExecuteScalar(parameters, "sp_registerUser");
            }
            return userId;
        }
        #endregion

        #region [Update Event]

        /// <summary>
        /// update event of user
        /// </summary>
        /// <param name="_event"></param>
        /// <returns>bool</returns>
        public bool UpdateEvent(Events _event)
        {
            bool updated = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("evntId", _event.EventId);
                parameters.Add("userId", _event.UserId);
                parameters.Add("eventType", _event.Type);
                parameters.Add("eventTitle", _event.Title);
                parameters.Add("templateId", _event.TemplateId);
                parameters.Add("eventMessage", _event.InvitationMessage);

                updated = db.ExecuteNonQuery(parameters, "sp_updateEvent");
            }
            return updated;
        }
        #endregion

        #region [Update SubEvent]

        /// <summary>
        /// updates subevent 
        /// </summary>
        /// <param name="subEvent"></param>
        /// <returns>bool</returns>
        public bool UpdateSubEvent(SubEvent subEvent) {
            bool updated = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header)) {

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("subEventId", subEvent.SubEventId);
                parameters.Add("evntId", subEvent.EventId);
                parameters.Add("subEventName", subEvent.SubEventName);
                parameters.Add("subEventDate", subEvent.SubEventDate);
                parameters.Add("strtTime", subEvent.StartTime);
                parameters.Add("endTime", subEvent.EndTime);
                parameters.Add("address", subEvent.Address);
                parameters.Add("lon", subEvent.Longitude);
                parameters.Add("lat", subEvent.Latitude);

                updated = db.ExecuteNonQuery(parameters, "sp_updateSubEvent");
            }
            return updated;
        }
        #endregion

        #region [Update Template]

        /// <summary>
        /// updates template 
        /// </summary>
        /// <param name="template"></param>
        /// <returns>bool</returns>
        public bool UpdateTemplate(Template template)
        {
            bool updated = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("templateId", template.TemplateId);
                parameters.Add("templateName", template.Name);
                parameters.Add("templateHtml", template.Html);
                parameters.Add("templateActive", template.Active.ToString());

                updated = db.ExecuteNonQuery(parameters, "sp_updateTemplate");
            }
            return updated;
        }
        #endregion

        #region [Update OTP]

        /// <summary>
        /// updates user otp 
        /// </summary>
        /// <param name="user"></param>
        /// <returns>bool</returns>
        public bool UpdateOtp(Users user)
        {
            bool updated = false;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {

                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("mob", Utility.Utility.header);
                parameters.Add("otp", user.Otp);
                parameters.Add("attempt", user.Attempt);
                parameters.Add("totalAttempt", user.TotalAttempt);
                parameters.Add("blockUser", user.IsBlock.ToString());

                updated = db.ExecuteNonQuery(parameters, "sp_updateUserOtp");
            }
            return updated;
        }
        #endregion

        #region [Validate Invitation]

        /// <summary>
        /// validates invitation is opened by right person or not
        /// returns "invited" or "not invited"
        /// </summary>
        /// <param name="invitedPeople"></param>
        /// <returns>string</returns>
        public string ValidateInvitation(InvitedPeople invitedPeople)
        {
            string status = string.Empty;

            if (!string.IsNullOrEmpty(Utility.Utility.header))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("userId", invitedPeople.UserId);
                parameters.Add("eventId", invitedPeople.EventId);
                parameters.Add("subEventId", invitedPeople.SubEventId);
                parameters.Add("invitedUser", invitedPeople.InvitedUser);

                status = db.ExecuteScalar(parameters, "sp_validateInvitation");
            }
            return status;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        #endregion


    }
}