﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace uInvited_Api.Models.Database
{
    public sealed class dbHelper
    {
        #region [Variables]
        MySqlConnection conn;
        MySqlCommand cmd;
        MySqlDataAdapter sda;
        #endregion

        #region [Constructor]
        public dbHelper()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["uinvited_db"].ConnectionString;
            conn = new MySqlConnection(connectionString);
        }
        #endregion

        #region [Open Connection]
        public MySqlConnection OpenConnection()
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            return conn;
        }
        #endregion

        #region [Close Connection]
        public MySqlConnection CloseConnection()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
            return conn;
        }

        #endregion

        #region [ExecuteReader]
        /// <summary>
        /// Returns rows from db
        /// </summary>
        /// <param name="parameters">Dictionary<string,string></param>
        /// <param name="procedureName">string</param>
        /// <returns>returns DataTable</returns>
        public DataTable ExecuteReader(Dictionary<string, string> parameters, string procedureName)
        {
            DataTable dt = new DataTable();
            try
            {
                OpenConnection();

                cmd = new MySqlCommand(procedureName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    foreach (KeyValuePair<string, string> param in parameters)
                    {
                        cmd.Parameters.AddWithValue(param.Key, param.Value);
                    }
                }

                sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);
            }
            finally
            {
                sda.Dispose();
                CloseConnection();
            }
            return dt;
        }
        #endregion

        #region [ExecuteNonQuery]
        /// <summary>
        /// Return success or failure after execution
        /// </summary>
        /// <param name="parameters">Dictionary<string,string></param>
        /// <param name="procedureName">string</param>
        /// <returns>returns true/false</returns>
        public bool ExecuteNonQuery(Dictionary<string, string> parameters, string procedureName)
        {
            bool status = false;

            try
            {
                OpenConnection();

                cmd = new MySqlCommand(procedureName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    foreach (KeyValuePair<string, string> param in parameters)
                    {
                        cmd.Parameters.AddWithValue(param.Key, param.Value);
                    }
                }

                int rowsEffected = cmd.ExecuteNonQuery();
                if (rowsEffected > 0)
                {
                    status = true;
                }
            }
            finally
            {
                CloseConnection();
            }
            return status;
        }
        #endregion

        #region [ExecuteScalar]
        /// <summary>
        /// returns first row first column value
        /// </summary>
        /// <param name="parameters">Dictionary<string,string></param>
        /// <param name="procedureName">string</param>
        /// <returns>returns string</returns>
        public string ExecuteScalar(Dictionary<string, string> parameters, string procedureName)
        {
            string value = string.Empty;

            try
            {
                OpenConnection();

                cmd = new MySqlCommand(procedureName, conn);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                {
                    foreach (KeyValuePair<string, string> param in parameters)
                    {
                        cmd.Parameters.AddWithValue(param.Key, param.Value);
                    }
                }

                value = (cmd.ExecuteScalar() ?? string.Empty).ToString();
            }
            finally
            {
                CloseConnection();
            }
            return value;
        }
        #endregion
    }
}