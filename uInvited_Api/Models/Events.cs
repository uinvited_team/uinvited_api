﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uInvited_Api.Models.Database;

namespace uInvited_Api.Models
{
    public class Events
    {
        #region [Attributes]
        public string EventId { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string TemplateId { get; set; }
        public string InvitationMessage { get; set; }
        public string UpdatedDate { get; set; }
        #endregion

        #region [Create Event]
        /// <summary>
        /// inserts the event created by user in database
        /// </summary>
        /// <param name="events">object</param>
        /// <returns>eventId</returns>
        public string CreateEvent(Events events)
        {
            string eventId = string.Empty;
            using (DbConnection db = new DbConnection())
            {
                eventId = db.CreateEvent(events);
            }
            return eventId;
        }
        #endregion

        #region [Update Event]
        /// <summary>
        /// update the user event
        /// </summary>
        /// <param name="events">Object</param>
        /// <returns>bool</returns>
        public bool UpdateEvent(Events events)
        {
            bool updated = false;
            using (DbConnection db = new DbConnection())
            {
                updated = db.UpdateEvent(events);
            }

            return updated;
        }
        #endregion

        #region [Delete Event]
        /// <summary>
        /// deletes event of user 
        /// </summary>
        /// <param name="eventId">string</param>
        /// <param name="userId">string</param>
        /// <returns>bool</returns>
        public bool DeleteEvent(string eventId, string userId)
        {
            bool deleted = false;
            using (DbConnection db = new DbConnection())
            {
                deleted = db.DeleteEvent(eventId, userId);
            }
            return deleted;
        }
        #endregion


    }
}