﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uInvited_Api.Models.Database;

namespace uInvited_Api.Models
{
    public class References
    {
        #region [Attributes]
        public string ReferencesId { get; set; }
        public string UserId { get; set; }
        public string EventId { get; set; }
        public string SubEventId { get; set; }
        public string ReferenceNo { get; set; }
        public string UpdatedDate { get; set; }
        #endregion

        #region [Create Reference]
        /// <summary>
        /// create reference 
        /// </summary>
        /// <param name="references"></param>
        /// <returns></returns>
        public References CreateReference(References references)
        {
            References newReference = new References();

            if (references != null)
            {
                references.ReferenceNo = Utility.Utility.MD5Hash(references.UserId + DateTime.Now);
            }

            using (DbConnection db = new DbConnection())
            {
                newReference = db.CreateReference(references);
            }
                return newReference;
        }
        #endregion
    }
}