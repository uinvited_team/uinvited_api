﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uInvited_Api.Models.Database;

namespace uInvited_Api.Models
{
    public class Template
    {
        #region [Attributes]
        public string TemplateId { get; set; }
        public string Name { get; set; }
        public string Html { get; set; }
        public bool Active { get; set; }
        public string UpdatedDate { get; set; }
        #endregion

        #region [Create Template]
        /// <summary>
        /// creates template
        /// </summary>
        /// <param name="template">object</param>
        /// <returns>templateId</returns>
        public string CreateTemplate(Template template)
        {
            string templateId = string.Empty;

            using (DbConnection db = new DbConnection())
            {
                templateId = db.CreateTemplate(template);
            }
            return templateId;
        }
        #endregion

        #region [Update Template]
        /// <summary>
        /// updates template
        /// </summary>
        /// <param name="template">object</param>
        /// <returns>bool</returns>
        public bool UpdateTemplate(Template template)
        {
            bool updated = false;

            using (DbConnection db = new DbConnection())
            {
                updated = db.UpdateTemplate(template);
            }
            return updated;
        }
        #endregion

        #region [Delete Template]
        /// <summary>
        /// deletes template
        /// </summary>
        /// <param name="templateId">string</param>
        /// <returns>bool</returns>
        public bool DeleteTemplate(string templateId)
        {
            bool deleted = false;

            using (DbConnection db = new DbConnection())
            {
                deleted = db.DeleteTemplate(templateId);
            }
            return deleted;
        }
        #endregion
    }
}
