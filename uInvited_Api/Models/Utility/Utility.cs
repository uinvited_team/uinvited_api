﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;

namespace uInvited_Api.Models.Utility
{
    public static class Utility
    {
        #region [Read Header]
        public static string header = string.Empty;
        public class RequestHandler : DelegatingHandler
        {
            protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                try
                {
                    header = request.Headers.GetValues("USERID").FirstOrDefault().ToString();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
                HttpResponseMessage response = await base.SendAsync(request, cancellationToken);
                return response;
            }


        }
        #endregion

        #region [Generate OTP]

        /// <summary>
        ///  generates a random otp consists of number & small alphas
        ///  where otp length is as configured in web config file.
        /// </summary>
        /// <returns>string</returns>
        public static string GenerateOTP()
        {
            string newOTP = string.Empty;
            int otpLength = int.Parse(ConfigurationManager.AppSettings["otp_length"]);
            Random random = new Random();

            char[] charArr = "0123456789abcdefghijklmnopqrstuvwxyz".ToCharArray();

            for (int i = 0; i < otpLength; i++)
            {
                //It will not allow Repetation of Characters
                int pos = random.Next(1, charArr.Length);
                if (!newOTP.Contains(charArr.GetValue(pos).ToString()))
                    newOTP += charArr.GetValue(pos);
                else
                    i--;
            }

            return newOTP;
        }
        #endregion

        #region ConvertToLocalDateTime
        public static string convertToLocal(string _datetime)
        {
            string convertedDateTime = string.Empty;
            if (!string.IsNullOrEmpty(_datetime))
            {
                try
                {
                    DateTime utcDateTime = DateTime.Parse(_datetime);
                    DateTime localDateTime = utcDateTime.ToLocalTime();
                    convertedDateTime = localDateTime.ToString();
                }
                catch (FormatException)
                {
                    convertedDateTime = "error in converting time";
                }
            }
            return convertedDateTime;
        }
        #endregion

        #region [Create MD5]
        /// <summary>
        /// generates MD5 of provided input
        /// </summary>
        /// <param name="input">string</param>
        /// <returns>string</returns>
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        #endregion

    }
}