﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uInvited_Api.Models.Database;

namespace uInvited_Api.Models
{
    public class Users
    {
        #region [Attributes]
        public string UserId { get; set; }
        public string MobileNo { get; set; }
        public string Otp { get; set; }
        public string Attempt { get; set; }
        public string TotalAttempt { get; set; }
        public bool IsBlock { get; set; }
        public string UpdatedDate { get; set; }
        #endregion

        #region [Register User]
        /// <summary>
        /// register user in database and generate otp
        /// </summary>
        /// <returns>otp or error message</returns>
        public string RegisterUser()
        {
            string message = string.Empty;
            using (DbConnection db = new DbConnection())
            {
                Users user = null;

                Random random = new Random();
                string newOtp = Utility.Utility.GenerateOTP();

                #region [Get Previous OTP]

                user = db.GetOTP();

                #endregion

                #region [New User]

                if (user == null)
                {
                    message = db.RegisterUser(newOtp);

                    if (!string.IsNullOrEmpty(message))
                    {
                        message = message + "~" + newOtp;
                    }
                }

                #endregion

                #region [Old User]

                else
                {
                    int totalAttempt = int.Parse(user.TotalAttempt);
                    int currentAttempt = int.Parse(user.Attempt);
                    TimeSpan duration = DateTime.Now - Convert.ToDateTime(user.UpdatedDate);
                    Users oldUser = null;

                    //if user is not blocked
                    if (!user.IsBlock)
                    {
                        if ((totalAttempt < 6) || ((totalAttempt % 6) != 0))
                        {
                            //if user current attempt is less then 3 then old otp will be used
                            //total attempt & current attempt will be increased by 1 
                            if (currentAttempt < 3)
                            {
                                oldUser = new Users();
                                oldUser.Otp = user.Otp;
                                oldUser.Attempt = (currentAttempt + 1).ToString();
                                oldUser.TotalAttempt = (totalAttempt + 1).ToString();
                                oldUser.IsBlock = false;
                            }

                            // if user current attempt exceeds 3 then duration will be checked
                            // if duration is 1 or greater than a new otp will be generated 
                            // and current attempt will be reset to 0
                            else
                            {
                                if (duration.Hours >= 1)
                                {
                                    oldUser = new Users();
                                    oldUser.Otp = newOtp;
                                    oldUser.Attempt = "0";
                                    oldUser.TotalAttempt = (totalAttempt + 1).ToString();
                                    oldUser.IsBlock = false;
                                }
                                else
                                {
                                    message = "otp trying limit exceeds. please, try after an hour.";
                                }
                            }
                        }
                        else
                        {
                            // if user exceeds the attempt limit by multiple of 6
                            // user will be allowed to attempt after 24 hours 
                            if (duration.Hours < 24)
                            {
                                oldUser = new Users();
                                oldUser.Otp = user.Otp;
                                oldUser.Attempt = currentAttempt.ToString();
                                oldUser.TotalAttempt = totalAttempt.ToString();
                                oldUser.IsBlock = true;
                                message = "otp trying limit exceeds. please, try after 24 hours.";
                            }
                            else
                            {
                                oldUser = new Users();
                                oldUser.Otp = newOtp;
                                oldUser.Attempt = "1";
                                oldUser.TotalAttempt = (totalAttempt + 1).ToString();
                                oldUser.IsBlock = false;

                                //update the user table will current attempt status & otp
                                if (oldUser != null)
                                {
                                    if (db.UpdateOtp(oldUser))
                                    {
                                        message = db.GetUserId();
                                        if (!string.IsNullOrEmpty(message))
                                        {
                                            message = message + "~" + newOtp;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            return message;
        }
        #endregion
    }
}