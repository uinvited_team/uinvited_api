﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using uInvited_Api.Models.Database;

namespace uInvited_Api.Models
{
    public class SubEvent
    {
        #region [Attributes]
        public string SubEventId { get; set; }
        public string EventId { get; set; }
        public string SubEventName { get; set; }
        public string SubEventDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Address { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string UpdatedDate { get; set; }
        #endregion

        #region [Create SubEvent]
        /// <summary>
        /// insert subEvent into database
        /// </summary>
        /// <param name="subEvent">Object</param>
        /// <returns>subEventId</returns>
        public string CreateSubEvent(SubEvent subEvent)
        {
            string subEventId = string.Empty;

            using (DbConnection db = new DbConnection())
            {
                subEventId = db.CreateSubEvent(subEvent);
            }
            return subEventId;
        }
        #endregion

        #region [Update SubEvent]
        /// <summary>
        /// updates subEvent 
        /// </summary>
        /// <param name="subEvent">object</param>
        /// <returns>bool</returns>
        public bool UpdateSubEvent(SubEvent subEvent)
        {
            bool updated = false;

            using (DbConnection db = new DbConnection())
            {
                updated = db.UpdateSubEvent(subEvent);
            }
            return updated;
        }
        #endregion

        #region [Delete SubEvent]
        /// <summary>
        /// deletes subEvent
        /// </summary>
        /// <param name="subEventId">string</param>
        /// <param name="eventId">string</param>
        /// <returns>bool</returns>
        public bool DeleteSubEvent(string subEventId, string eventId)
        {
            bool deleted = false;

            using (DbConnection db = new DbConnection())
            {
                deleted = db.DeleteSubEvent(subEventId, eventId);
            }
            return deleted;
        }
        #endregion

    }
}