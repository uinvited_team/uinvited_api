﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using uInvited_Api.Models.Database;

namespace uInvited_Api.Models
{
    public class InvitedPeople
    {
        #region [Attributes]
        public string InvitedPeopleId { get; set; }
        public string UserId { get; set; }
        public string EventId { get; set; }
        public string SubEventId { get; set; }
        public string InvitedUser { get; set; }
        public bool Accepted { get; set; }
        public string UpdatedDate { get; set; }
        #endregion

        #region [Invite People]
        /// <summary>
        /// add invited people
        /// </summary>
        /// <param name="invitePeople">Object</param>
        /// <returns>bool</returns>
        public bool InvitePeople(InvitedPeople invitePeople)
        {
            bool invited = false;

            using (DbConnection db = new DbConnection())
            {
                invited = db.InvitePeople(invitePeople);
            }
                return invited;
        }
        #endregion

        #region [Get Invited People List]
        /// <summary>
        /// Get invited people list
        /// </summary>
        /// <param name="invitedPeople">Object</param>
        /// <returns>list of invited people</returns>
        public List<InvitedPeople> GetInvitedPeople(InvitedPeople invitedPeople)
        {
            List<InvitedPeople> invitedPeopleList = new List<InvitedPeople>();

            using (DbConnection db = new DbConnection())
            {
                invitedPeopleList = db.GetInvitedPeople(invitedPeople);
            }

            return invitedPeopleList;
        }
        #endregion
    }
}